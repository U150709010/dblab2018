SELECT * FROM company.customers;

describe customers;

select *
from company.customers
order by customers.CustomerName desc;

create index abc on customers(CustomerName);

explain select *
from company.customers
order by customers.CustomerName desc;

alter table customers drop index abc;

create view myView as
select *
from company.customers
order by customers.CustomerName desc;

select *
from myView;

insert into myView
values('92','Wilmoon','Plesen','asdasdasd','Muğla','48000','Turkey');

select *
from myView;
