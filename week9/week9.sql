select count(customers.CustomerID) as numberOfCustomers, customers.Country
from customers
group by customers.Country
order by numberOfCustomers desc;

select count(products.ProductID) as numberOfProducts, suppliers.SupplierName
from products join suppliers on products.SupplierID = suppliers.SupplierID
group by products.SupplierID
order by numberOfProducts desc;

select *
from customers;

select * from employees;

show variables like 'secure_file_priv';

load data local infile "C:\\ProgramData\\MySQL\\MySQL Server 5.6\\Uploads\\customers.csv"
into table customers
fields terminated by ';'
ignore 1 lines;


load data local infile "C:\\ProgramData\\MySQL\\MySQL Server 5.6\\Uploads\\employees.csv"
into table employees
fields terminated by ','
ignore 1 lines;


load data local infile "C:\\ProgramData\\MySQL\\MySQL Server 5.6\\Uploads\\categories.tsv"
into table categories;
