#1-) What is the supplier and its country that lent the maximum amont of Money ?
select supplier_name, supplier country, total_amount
from supplier join loan on supplier.id = loan.id
order by total_amount desc
limit 1;


#3-) What is the most borrowed amount?
select total_amount
from loan
order by total_amount desc
limit 1;


#5-) what is the supplier country code which country name is Mali	
select supplier_country_code
from supplier
where supplier_country = "Mali";


#7-) What are the projects that have more than  $50000 contract amount?
select project_name, total_amount
from project join loan on project.id = loan.id
where total_amount > 50000 
order by total_amount ;

#9-)Which procurement method is the most used?
select count(procurement_method) as NumberOfMethod
from procurement
group by procurement_method
order by NumberOfMethod desc
limit 1;
